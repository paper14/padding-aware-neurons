import os

PROJECT_PATH = os.path.abspath(os.path.join(__file__, *(os.pardir for _ in range(2))))
# DATASETS
DATASETS_PATH = os.path.join(PROJECT_PATH, "original_dataset")
