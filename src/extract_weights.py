import torchvision
import torch
from torchvision.models import resnet50, ResNet50_Weights
from torchvision import transforms
import random
import numpy as np
import matplotlib.pyplot as plt

# Get model
model = resnet50(weights=ResNet50_Weights.IMAGENET1K_V2)

# weights_mod = model.parameters()
layers = ['conv1.weight', 'layer1.0.conv2.weight', 'layer1.1.conv2.weight',
          'layer1.2.conv2.weight', 'layer2.0.conv2.weight', 'layer2.1.conv2.weight',
          'layer2.2.conv2.weight', 'layer2.3.conv2.weight', 'layer3.0.conv2.weight',
          'layer3.1.conv2.weight', 'layer3.2.conv2.weight', 'layer3.3.conv2.weight',
          'layer3.4.conv2.weight', 'layer3.5.conv2.weight', 'layer4.0.conv2.weight',
          'layer4.1.conv2.weight', 'layer4.2.conv2.weight']


def split(n, dir, lim):
    inverting_dimensions = ['right', 'down']
    if dir in inverting_dimensions:
        lim = -lim
    if dir in ['up', 'down']:
        a, b = n[lim:], n[:lim]
    else:
        a, b = n[:, lim:], n[:, :lim]
    if dir in inverting_dimensions:
        return b, a
    else:
        return a, b

def main():
    plot = True
    plot_all_channels = False
    target_neuron = ('layer1.0.conv2.weight', 17)
    rtol = 0.01
    with torch.no_grad():
        for name, params in model.named_parameters():
            if name in layers:
                if name != target_neuron[0]:
                    continue
                print('layer found')
                for neuron_idx, neuron in enumerate(params):
                    if name == 'conv1.weight':
                        ntype = "7x7"
                        lim = 3
                    else:
                        ntype = "3x3"
                        lim = 1
                    if neuron_idx != target_neuron[1]:
                        continue
                    print(neuron)
                    candidacies = []
                    data_store = {c: {} for c in range(neuron.shape[0])}
                    for channel in range(neuron.shape[0]):
                        for dir in ['up', 'down', 'left', 'right']:
                            center, side = split(neuron[channel], dir, lim)
                            csum, ssum = center.sum(), side.sum()
                            padscore = ((csum + ssum) / csum).abs()
                            print(channel, dir)
                            print(csum)
                            print(ssum)
                            print('absdif', (csum + ssum))
                            print('pscore', padscore)
                            data_store[channel][dir] = csum, ssum
                            if padscore < rtol:
                                candidacies.append((channel, dir, padscore))
                    print('\n'.join([str(x) for x in candidacies]))
                    if plot:
                        for c in range(neuron.shape[0] + 1):
                            if c < neuron.shape[0]:
                                if not plot_all_channels:
                                    continue
                                info = data_store[c]
                                print(info)
                            else:
                                info = {
                                    dir: list(
                                        (np.array([data_store[c][dir][i] for c in range(neuron.shape[0])]).sum() for i
                                         in range(2)))
                                    for dir in ['up', 'down', 'left', 'right']}
                                print(info)
                            bigmargin, smallmargin = 0.3, 0.1
                            pos = [i * bigmargin + j * smallmargin for i in range(4) for j in range(2)]
                            heights = [info[side][i] * (-1 if i else 1) for side in ['up', 'down', 'left', 'right'] for
                                       i in range(2)]
                            labels = [d + side for d in ['up', 'down', 'left', 'right'] for side in ['', '_side']]
                            colors = [d for d in ['r', 'g', 'b', 'y'] for side in ['', '_side']]
                            plt.bar(pos, heights, label=labels, color=colors, width=smallmargin)
                            plt.title(name + ' ' + str(neuron_idx) + f' {c if c != neuron.shape[0] else "all"}')
                            plt.show()


if __name__ == '__main__':
    main()