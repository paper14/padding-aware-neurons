from matplotlib import pyplot as plt
import numpy as np
import scipy
import scipy.stats
import matplotlib
from matplotlib import colors
from copy import copy

def get_acc_dict(file):
    try:
        with open(file, 'r') as f:
            data = [(l.split(': ')[0], float(l.split(': ')[1])) for l in f.read().split('\n')]
        return data
    except FileNotFoundError:
        err_msg = f"File f{file} not found." +\
                  "This code needs the data from E.compute_imagenet_accuracy.py executed for zeros (default)," + \
                  "reflect, and out_reflect parameters to run."
        raise ValueError(err_msg)

def cmap_map(function, cmap):
    """ Applies function (which should operate on vectors of shape 3: [r, g, b]), on colormap cmap.
    This routine will break any discontinuous points in a colormap.
    """
    cdict = cmap._segmentdata
    step_dict = {}
    # Firt get the list of points where the segments start or end
    for key in ('red', 'green', 'blue'):
        step_dict[key] = list(map(lambda x: x[0], cdict[key]))
    step_list = sum(step_dict.values(), [])
    step_list = np.array(list(set(step_list)))
    # Then compute the LUT, and apply the function to the LUT
    reduced_cmap = lambda step : np.array(cmap(step)[0:3])
    old_LUT = np.array(list(map(reduced_cmap, step_list)))
    new_LUT = np.array(list(map(function, old_LUT)))
    # Now try to make a minimal segment definition of the new LUT
    cdict = {}
    for i, key in enumerate(['red','green','blue']):
        this_cdict = {}
        for j, step in enumerate(step_list):
            if step in step_dict[key]:
                this_cdict[step] = new_LUT[j, i]
            elif new_LUT[j,i] != old_LUT[j, i]:
                this_cdict[step] = new_LUT[j, i]
        colorvector = list(map(lambda x: x + (x[1], ), this_cdict.items()))
        colorvector.sort()
        cdict[key] = colorvector

    return matplotlib.colors.LinearSegmentedColormap('colormap',cdict,1024)


def get_acc_difs(acc1, acc2):
    return np.array([x[1]-y[1] for x, y in zip(acc1, acc2)])


def jitter_data(data):
    return data + np.random.normal(0, 0.001, len(data))

def delta_acc_to_pos(d_acc, dist, min):
    return round((d_acc-min)/dist)

def class_accuracy_2d_histogram(reflect_decay, pan_decay):
    range_vals = pan_decay.max() - pan_decay.min(), reflect_decay.max() - reflect_decay.min()
    dist = 0.02
    img_size = (int(range_vals[0]/dist)+1, int(range_vals[1]/dist)+1)
    matrix_data = np.ones(img_size)*0.001
    for reflect_d, pan_d in zip(reflect_decay.tolist(), pan_decay.tolist()):
        matrix_position = img_size[0]-1-delta_acc_to_pos(pan_d, dist, pan_decay.min()), \
            delta_acc_to_pos(reflect_d, dist, reflect_decay.min())
        matrix_data[matrix_position] += 1

    plt.imshow(np.log(matrix_data), cmap='viridis')
    xticks = [i for i in range(0,img_size[1],2)]
    plt.xticks(xticks, (np.array(xticks)*dist+reflect_decay.min()).round(2))
    yticks = [i for i in range(0,img_size[0],2)]
    plt.yticks(yticks, (np.array(yticks)*dist+pan_decay.min()).round(2)[::-1])
    plt.colorbar(location='bottom')
    plt.show()

def exp():
    zero = get_acc_dict('acc_store_zeros')
    refl = get_acc_dict('acc_store_reflect')
    pan_acc = get_acc_dict('acc_store_out_reflect_new_pans')
    pan_acc_rand = get_acc_dict('acc_store_random')
    pan_acc_rand = get_acc_dict('acc_store_random2')

    reflect_delta = get_acc_difs(refl, zero)
    panreflect_delta = get_acc_difs(pan_acc, zero)
    randreflect_delta = get_acc_difs(pan_acc_rand, zero)

    histogram2d(panreflect_delta, reflect_delta)
    histogram2d(randreflect_delta, reflect_delta)
    classes_that_change_a_lot = (np.abs(panreflect_delta) > 0.02)
    different_classes1 = np.array(zero)[classes_that_change_a_lot]
    different_classes2 = np.array(refl)[classes_that_change_a_lot]
    different_classes3 = np.array(pan_acc)[classes_that_change_a_lot]


    print("corr:", scipy.stats.pearsonr(reflect_delta, randreflect_delta))
    zero_accs = np.array([acc for c, acc in zero])
    pan_accs = np.array([acc for c, acc in pan_acc])

    bins = np.arange(-0.10, 0.08, 0.02)
    v, b, _ = plt.hist(panreflect_delta, bins)
    vr, br, _ = plt.hist(randreflect_delta, bins)
    plt.show()
    print(panreflect_delta.max())
    print((panreflect_delta<-0).sum())
    print((randreflect_delta<-0.5).sum())
    plt.plot(b[1:],v/1000)
    plt.plot(br[1:],vr/1000)
    plt.show()


def histogram2d(panreflect_delta, reflect_delta):
    numbins = round((reflect_delta.max() - reflect_delta.min()) * 50) + 1, \
              round((panreflect_delta.max() - panreflect_delta.min()) * 50) + 1
    cmap_copy = copy(matplotlib.cm.get_cmap('viridis'))
    cmap_copy.set_bad((0, 0, 0))
    plt.hist2d(reflect_delta, panreflect_delta, bins=numbins, cmap=cmap_copy,
               norm=colors.LogNorm())
    plt.colorbar(location='bottom')
    plt.xticks(np.arange(-0.32, 0.14, step=0.04))
    plt.show()


if __name__=='__main__':
    exp()