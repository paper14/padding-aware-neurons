import argparse
import numpy as np
import torch
import torchvision
from torch import nn
from torchvision.models import resnet50, ResNet50_Weights, mobilenet_v3_large, MobileNet_V3_Large_Weights,\
    googlenet, GoogLeNet_Weights,\
    efficientnet_b0, EfficientNet_B0_Weights, densenet121, DenseNet121_Weights,\
    inception_v3, Inception_V3_Weights
from torchvision.models.detection import fasterrcnn_mobilenet_v3_large_fpn, FasterRCNN_MobileNet_V3_Large_FPN_Weights,\
    fcos_resnet50_fpn, FCOS_ResNet50_FPN_Weights
    #FCN_ResNet50_Weights.COCO_WITH_VOC_LABELS_V1 (semantic segmentation)
    #FasterRCNN_MobileNet_V3_Large_FPN_Weights.COCO_V1 (object detection)
    #KeypointRCNN_ResNet50_FPN_Weights.COCO_V1 (keypoint detection)
        
from src.paths import DATASETS_PATH

def get_conv2ds_kernel_larger_than(model, kernel_size=(3, 3)):
    conv_names_over_nxn, conv_layers_over_nxn = zip(*[(n, l)
                                                      for n, l in model.named_modules()
                                                      if isinstance(l, nn.Conv2d)
                                                      and l.weight.shape[2] >= kernel_size[0]
                                                      and l.weight.shape[3] >= kernel_size[1]])
    return conv_names_over_nxn, conv_layers_over_nxn

def model_transform_padding(layer_list, padding_mode):
    if padding_mode not in {'zeros', 'reflect', 'replicate', 'circular'}:
        print("Padding not understood", padding_mode)
        raise NotImplementedError()
    for layer in layer_list:
        layer.padding_mode = padding_mode

def act_dict(layer_names):
    return {n: [] for n in layer_names}

def get_hook_function(name):
    # the hook signature
    def hook(model, input, output):
        global activation
        activation[name] = output.detach().cpu().numpy()

    return hook

def remove_hooks(hooks):
    for key in hooks:
        hooks[key].remove()

def add_hooks(layer_names, layers):
    return {n: l.register_forward_hook(get_hook_function(n)) for n, l in zip(layer_names, layers)}

def get_model(model_arch):
    # Get model
    if model_arch == 'resnet50':
        weights = ResNet50_Weights.IMAGENET1K_V2
        model = resnet50(weights=weights)
    elif model_arch == 'mobilenet_v3_large':
        weights = MobileNet_V3_Large_Weights.IMAGENET1K_V2
        model = mobilenet_v3_large(weights=weights)
    elif model_arch == 'densenet121':
        weights = DenseNet121_Weights.IMAGENET1K_V1
        model = densenet121(weights=weights)
    elif model_arch == 'googlenet':
        weights = GoogLeNet_Weights.IMAGENET1K_V1
        model = googlenet(weights=weights)
    elif model_arch == 'fasterrcnn':
        weights = FasterRCNN_MobileNet_V3_Large_FPN_Weights.COCO_V1
        model = fasterrcnn_mobilenet_v3_large_fpn(weights=weights)
    elif model_arch == 'fcos_resnet':
        weights = FCOS_ResNet50_FPN_Weights.COCO_V1
        model = fcos_resnet50_fpn(weights=weights)
    else:
        raise Exception('Model architecture not recognized:'+str(model_arch)+'\n'+\
            'Consider add it to the extract_activations.get_model method')
    return model, weights

def get_transforms(model_arch, weights):
    #Some models dont uniformize data prior to input. For these cases use the ResNet transform.
    if model_arch in ['fasterrcnn','fcos_resnet']:
        transform = ResNet50_Weights.IMAGENET1K_V2.transforms()
    else:
        transform = weights.transforms()
    return transform

def inference_setup(model, transform, batch_size, padding_mode):
    #Eval mode
    model.eval()
    # Get data & loader for evaluation
    caltech_data = torchvision.datasets.Caltech101(root=DATASETS_PATH,
                                                   transform=transform, download=True)
    data_loader = torch.utils.data.DataLoader(caltech_data, batch_size=batch_size,
                                              shuffle=False)
    # Modify padding
    conv_names, conv_layers = get_conv2ds_kernel_larger_than(model, (3, 3))
    model_transform_padding(conv_layers, padding_mode)
    # Init device & model
    device = torch.device('cuda') if torch.cuda.is_available() \
        else torch.device('cpu')
    model = model.to(device)
    # Prepare hooks and activation structs
    global activation
    activation = {}
    act_lists = act_dict(conv_names)
    hooks = add_hooks(conv_names, conv_layers)
    return act_lists, hooks, data_loader

def run_inference(data_loader, model, act_lists, batch_size, sample_size):
    # Extract activations
    print('Extracting...')
    try:
        for X, y in data_loader:
            if sample_size == 0:
                break
            # forward pass -- getting the outputs
            out = model(X)
            # collect the activations in the correct list
            for layer_name in act_lists:
                if len(act_lists[layer_name]) == 0:
                    act_lists[layer_name] = activation[layer_name].astype(np.float16).copy()
                else:
                    act_lists[layer_name] = np.append(activation[layer_name].astype(np.float16), act_lists[layer_name], axis=0)
            sample_size -= batch_size
    except RuntimeError:
        raise Exception('Transforms of this model does not uniformize input. Consider using another one in method get_transforms')
    print('Success. Saving. Bye.')

def persist(model_arch, padding_mode, act_lists, hooks):
    # Persist activations
    import pickle
    with open(model_arch+'_activations_' + padding_mode + '.pkl', 'wb') as f:
        pickle.dump(act_lists, f)
    # detach the hooks
    remove_hooks(hooks)

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [PARAM1] [PARAM2]...",
        description="Extract activations produced by a batch of data in '\
            'a pre-trained model given a set of parameters."
    )
    parser.add_argument(
        "-ss", "--sample_size", default = 256, type = int, \
        help = 'Total number of samples to be used for activation extraction'
    )
    parser.add_argument(
        "-bs", "--batch_size", default = 32, type = int, \
        help = 'Size of data to be process concurrently'
    )
    parser.add_argument(
        "-pad", "--padding_type", default = 'zeros', type = str, \
        help = 'Type of padding to be used. Options are zeros, reflect, replicate and circular'
    )
    parser.add_argument(
        "-model", "--model_architecture", default = 'resnet50', type = str, \
        help = 'Pre-trained model to be used. Options are resnet50, densenet121,'\
            ' mobilenet_v3_large, googlenet, fasterrcnn, fcos_resnet'
    )
    return parser

#MAIN
def main():
    #parse params
    parser = init_argparse()
    args = parser.parse_args()
    #Get model
    model, weights = get_model(args.model_architecture)
    transforms = get_transforms(args.model_architecture, weights)
    act_lists, hooks, data_loader = inference_setup(model, transforms, args.batch_size, args.padding_type)
    run_inference(data_loader, model, act_lists, int(args.batch_size), args.sample_size)
    persist(args.model_architecture, args.padding_type, act_lists, hooks)
    return

if __name__ == "__main__":
    main()