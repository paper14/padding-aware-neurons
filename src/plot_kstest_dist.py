import pickle
import matplotlib.pyplot as plt
import matplotlib as mpl
from functools import reduce
import numpy as np

####################
#### PARAMETERS ####
####################

ks_data_file = 'ks_signed_center_two-sided.pkl'

####################
####################

#Read pre-computed data
kstest_data = {}
with open(ks_data_file, 'rb') as f:
    kstest_data = pickle.load(f)

ks_by_layer = {}
for layer in kstest_data:
    ks_by_layer[layer] = {}
    for direction in [(0,'top'), (1,'bot'), (2,'lef'), (3,'rig')]:
        ks_by_layer[layer][direction[1]] = []
        for neuron in kstest_data[layer]:
            ks_by_layer[layer][direction[1]].append(neuron[direction[0]][0])

plots_per_row = 6
total_plots = len(ks_by_layer)
plt.rc('ytick', labelsize=14)
plt.rc('xtick', labelsize=14)
fig, axs = plt.subplots(nrows=round(total_plots/plots_per_row), ncols=plots_per_row,figsize=(20, 10))
mpl.rcParams['patch.linewidth'] = 3
plt.rcParams.update({'font.size': 14})
axs = axs.ravel()
for layer, ax in zip(kstest_data,axs):
    ax.hist([ks_by_layer[layer]['top'],\
            ks_by_layer[layer]['bot'],\
            ks_by_layer[layer]['lef'],\
            ks_by_layer[layer]['rig']],\
       bins=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], histtype='barstacked', label = ["Top","Botton","Left","Right"])
    ax.xaxis.grid(True, which='both')
    ax.set_title(layer)
    ax.set_xlim([0, 1])
    plt.xticks([0,0.5,1])
plt.tight_layout()
#plt.legend(["Top","Botton","Left","Right"])
#axs[0].legend(loc="upper right")
plt.show()
    