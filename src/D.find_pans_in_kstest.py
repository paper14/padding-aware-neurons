import argparse
import pickle
from functools import reduce
from re import I
import matplotlib.pyplot as plt
import matplotlib as mpl
from torch import threshold
import numpy as np
from operator import add 

def read_data(name):
    #Read pre-computed data
    kstest_data = {}
    with open(name, 'rb') as f:
        kstest_data = pickle.load(f)
    return kstest_data


def find_4kind_pans(kstest_data, threshold):
    #Find PAN candidates
    candidates = {}
    for layer in kstest_data:
        layer_candidates = {'top':[],'bottom':[],'left':[],'right':[]}
        #print('Working on layer',layer)
        for neuron_idx, neuron in enumerate(kstest_data[layer]):
            if neuron[0][0] > threshold:
                #print('TOP ->',neuron_idx,neuron[0])
                layer_candidates['top'].append(neuron_idx)
            if neuron[1][0] > threshold:
                #print('BOT ->',neuron_idx,neuron[1])
                layer_candidates['bottom'].append(neuron_idx)
            if neuron[2][0] > threshold:
                #print('LEF ->',neuron_idx,neuron[2])
                layer_candidates['left'].append(neuron_idx)
            if neuron[3][0] > threshold:
                #print('RIG ->',neuron_idx,neuron[3])
                layer_candidates['right'].append(neuron_idx)
        candidates[layer] = layer_candidates
    return candidates

def find_15kind_pans(candidates):
    all_lays = {}
    for layer in candidates:
        #print('\nLayer',layer,'has',len(kstest_data[layer]),'neurons')
        set_of_candidates = {}
        set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['bottom'])).intersection(set(candidates[layer]['left'])).intersection(set(candidates[layer]['right']))

        set_of_candidates['TOP-BOTTOM-LEFT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['bottom'])).intersection(set(candidates[layer]['left']))
        set_of_candidates['TOP-BOTTOM-LEFT'] = set_of_candidates['TOP-BOTTOM-LEFT'] -\
             set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] 

        set_of_candidates['TOP-BOTTOM-RIGHT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['bottom'])).intersection(set(candidates[layer]['right']))
        set_of_candidates['TOP-BOTTOM-RIGHT'] = set_of_candidates['TOP-BOTTOM-RIGHT'] -\
             set_of_candidates['TOP-BOTTOM-LEFT-RIGHT']

        set_of_candidates['TOP-LEFT-RIGHT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['left'])).intersection(set(candidates[layer]['right']))
        set_of_candidates['TOP-LEFT-RIGHT'] = set_of_candidates['TOP-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT']

        set_of_candidates['BOTTOM-LEFT-RIGHT'] = set(candidates[layer]['bottom']).intersection(set(candidates[layer]['left'])).intersection(set(candidates[layer]['right']))
        set_of_candidates['BOTTOM-LEFT-RIGHT'] = set_of_candidates['BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT']

        set_of_candidates['TOP-BOTTOM'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['bottom']))
        set_of_candidates['TOP-BOTTOM'] = set_of_candidates['TOP-BOTTOM'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT']

        set_of_candidates['TOP-LEFT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['left']))
        set_of_candidates['TOP-LEFT'] = set_of_candidates['TOP-LEFT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['TOP-LEFT-RIGHT']

        set_of_candidates['TOP-RIGHT'] = set(candidates[layer]['top']).intersection(set(candidates[layer]['right']))
        set_of_candidates['TOP-RIGHT'] = set_of_candidates['TOP-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT'] -\
            set_of_candidates['TOP-LEFT-RIGHT']

        set_of_candidates['BOTTOM-LEFT'] = set(candidates[layer]['bottom']).intersection(set(candidates[layer]['left']))
        set_of_candidates['BOTTOM-LEFT'] = set_of_candidates['BOTTOM-LEFT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT']

        set_of_candidates['BOTTOM-RIGHT'] = set(candidates[layer]['bottom']).intersection(set(candidates[layer]['right']))
        set_of_candidates['BOTTOM-RIGHT'] = set_of_candidates['BOTTOM-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT']

        set_of_candidates['LEFT-RIGHT'] = set(candidates[layer]['left']).intersection(set(candidates[layer]['right']))
        set_of_candidates['LEFT-RIGHT'] = set_of_candidates['LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-LEFT-RIGHT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT']

        set_of_candidates['TOP'] = set(candidates[layer]['top'])
        set_of_candidates['TOP'] = set_of_candidates['TOP'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['TOP-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM'] -\
            set_of_candidates['TOP-LEFT'] -\
            set_of_candidates['TOP-RIGHT']

        set_of_candidates['BOTTOM'] = set(candidates[layer]['bottom'])
        set_of_candidates['BOTTOM'] = set_of_candidates['BOTTOM'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM'] -\
            set_of_candidates['BOTTOM-LEFT'] -\
            set_of_candidates['BOTTOM-RIGHT']

        set_of_candidates['LEFT'] = set(candidates[layer]['left'])
        set_of_candidates['LEFT'] = set_of_candidates['LEFT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT'] -\
            set_of_candidates['TOP-LEFT-RIGHT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-LEFT'] -\
            set_of_candidates['BOTTOM-LEFT'] -\
            set_of_candidates['LEFT-RIGHT']

        set_of_candidates['RIGHT'] = set(candidates[layer]['right'])
        set_of_candidates['RIGHT'] = set_of_candidates['RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-BOTTOM-RIGHT'] -\
            set_of_candidates['TOP-LEFT-RIGHT'] -\
            set_of_candidates['BOTTOM-LEFT-RIGHT'] -\
            set_of_candidates['TOP-RIGHT'] -\
            set_of_candidates['BOTTOM-RIGHT'] -\
            set_of_candidates['LEFT-RIGHT']
        all_lays[layer] = set_of_candidates
    return all_lays

def extract_layer_sizes(filename):
    data = read_data(filename)
    sizes = {}
    for layer in data:
        sizes[layer] = len(data[layer])
    return sizes

def compute_proportion_and_depth(candidates, proportional, layer_sizes = []):
    if proportional and len(layer_sizes) == 0:
        raise Exception("Setting proportional to True requires the list of sizes as parameter")
    data_prop = []
    for layer in candidates:
        agg_layer_cand = set()
        for subtype in candidates[layer]:
            agg_layer_cand = agg_layer_cand.union(candidates[layer][subtype])
        if proportional:
            data_prop.append(len(agg_layer_cand)/layer_sizes[layer])
        else:
            data_prop.append(len(agg_layer_cand))
    return data_prop

def process_kstest_data(filename, threshold):
    kstest_data = read_data(filename)
    candidates = find_4kind_pans(kstest_data, threshold)
    return candidates
    
def filter_candidates(candidates,candidates2):
    #Filter both    
    filtered = {}    
    for layer in candidates:    
        filtered[layer] = {}
        for pos in candidates[layer]:    
            filtered[layer][pos] = list(set(candidates[layer][pos]).intersection(set(candidates2[layer][pos])))    
    return filtered   

def merge_candidates(candidates,candidates2):
    #Merge    
    merged = {}    
    for layer in candidates:    
        merged[layer] = {}
        for pos in candidates[layer]:    
            merged[layer][pos] = list(set(candidates[layer][pos]).union(set(candidates2[layer][pos])))    
    return merged

def get_subtype_distribution(pan_candidates):
    subtype_candidates = find_15kind_pans(pan_candidates)
    type_pans = dict.fromkeys(['TOP','BOTTOM','LEFT', 'RIGHT', 'TOP-BOTTOM',\
            'TOP-LEFT','TOP-RIGHT', 'BOTTOM-LEFT', 'BOTTOM-RIGHT', 'LEFT-RIGHT',\
            'TOP-BOTTOM-LEFT', 'TOP-BOTTOM-RIGHT', 'TOP-LEFT-RIGHT',\
            'BOTTOM-LEFT-RIGHT', 'TOP-BOTTOM-LEFT-RIGHT'],0)
    for l in subtype_candidates:
        for t in subtype_candidates[l]:
            if subtype_candidates[l][t]!=set():
                type_pans[t] += len(subtype_candidates[l][t])
    return type_pans

def plot_histogram(num_layers, all_edges, prop_edges, all_pans, prop_pans):
    def addlabels(x,y,s):
        for i in range(len(x)):
            plt.text(x[i],y[i]+0.1,str(s[i])+'%', fontdict = {'weight': 'bold','size': 18,})
    plt.rc('ytick', labelsize=14)
    plt.rc('xtick', labelsize=14)
    mpl.rcParams['patch.linewidth'] = 5
    plt.rcParams.update({'font.size': 14})
    barWidth = 0.5
    plt.bar(np.arange(num_layers)+.85,all_edges, color ='r', width = barWidth, label ='Edge Detectors')
    addlabels(np.arange(num_layers)+.6, all_edges, prop_edges)
    plt.bar(np.arange(num_layers)+1.15,all_pans, color ='b', width = barWidth, label ='Padding Aware Neurons')
    addlabels(np.arange(num_layers)+1,all_pans,prop_pans)
    plt.xlim(xmin=0.5)    
    plt.ylim(ymin=0)
    plt.xticks(np.arange(1,num_layers+1,1))
    for pos in np.arange(num_layers+1):
        plt.axvline(x=pos)
    plt.legend(loc='upper left')
    plt.xlabel('Layer depth')
    plt.ylabel('Frequency')
    plt.tight_layout()
    plt.show()
    return

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [PARAM1] [PARAM2]...",
        description="Extract activations produced by a batch of data in '\
            'a pre-trained model given a set of parameters."
    )
    parser.add_argument(
        "-th", "--ks_threshold", type = float, required = True, \
        help = 'KS thrshold used to discriminate PANs. Theta in the PAN paper.'
    )
    parser.add_argument(
        "-model", "--model_architecture", default = 'resnet50', type = str, \
        help = 'Pre-trained model to be used. Options are resnet50, densenet121,'\
            ' mobilenet_v3_large, googlenet, fasterrcnn, fcos_resnet'
    )
    parser.add_argument(
        "-pad", "--padding_type", default = 'zeros', type = str, \
        help = 'Type of padding to be used. Options are zeros, reflect, replicate and circular'
    )
    parser.add_argument(
        "-plot", "--generate_plot", default = False, type = bool,\
        help = 'To generate and show a layerwise histogram of PANs, set this option to true.'
    )
    parser.set_defaults(add_minus=False, add_maxus=False)
    return parser

#MAIN
def main():
    #parse params
    parser = init_argparse()
    args = parser.parse_args()
    #set up files'
    filename_complete = 'ks_'+args.model_architecture+'_complete_two-sided_'+\
        args.padding_type+'.pkl'
    filename_bottom = 'ks_'+args.model_architecture+'_bottom_greater_'+\
        args.padding_type+'.pkl'
    filename_top = 'ks_'+args.model_architecture+'_top_less_'+\
        args.padding_type+'.pkl'

    layer_sizes = extract_layer_sizes(filename_complete)
    num_layers = len(layer_sizes)
    #Process each file
    edge_candidates = process_kstest_data(filename_complete, args.ks_threshold)
    pan_bottom_candidates = process_kstest_data(filename_bottom, args.ks_threshold)
    pan_top_candidates = process_kstest_data(filename_top, args.ks_threshold)
    #Get intersection for PANs
    pan_bottom_candidates_filtered = filter_candidates(pan_bottom_candidates, edge_candidates)
    pan_top_candidates_filtered = filter_candidates(pan_top_candidates, edge_candidates)
    #Union to get all PANs together
    pan_candidates = merge_candidates(pan_bottom_candidates_filtered, pan_top_candidates_filtered)
    print('PAN candidates')
    print(pan_candidates)
    all_edges = compute_proportion_and_depth(edge_candidates, False, layer_sizes)
    all_pans = compute_proportion_and_depth(pan_candidates, False, layer_sizes)
    #print("Absolute number of EDs found:")
    #print(all_edges)
    #print('Total:',sum(all_edges))
    print("Absolute number of PANs found:")
    print(all_pans)
    print('Total:',sum(all_pans))
    prop_edges = compute_proportion_and_depth(edge_candidates,True, layer_sizes)
    for i,x in enumerate(prop_edges):
        prop_edges[i] = int((str(x)+'0')[2:4])
    prop_pans = compute_proportion_and_depth(pan_candidates, True, layer_sizes)
    for i,x in enumerate(prop_pans):
        prop_pans[i] = int((str(x)+'0')[2:4])
    print("Layer sizes:",layer_sizes)
    #print("Relative number of EDs found")
    #print(prop_edges)
    print("Percentatge of PANs found (rounded to int)")
    print(prop_pans)
    subtypes = get_subtype_distribution(pan_candidates)
    print('Distribution of PAN subtypes:'+str(subtypes))
    if args.generate_plot:
        plot_histogram()
    return

if __name__ == "__main__":
    main()



