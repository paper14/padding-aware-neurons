import argparse

import torchvision
import torch
from torchvision.models import resnet50, ResNet50_Weights
from torchvision import transforms
import random
from matplotlib import pyplot as plt
import numpy as np
from src.paths import DATASETS_PATH
from src.IN_labels import class_list


def get_layers(model):
    list = [model.conv1]
    for layer_set in [model.layer1, model.layer2, model.layer3, model.layer4]:
        list += [block.conv2 for block in layer_set]
    return list


def model_transform_padding(layer_list, padding_mode, target_neurons=None):
    if padding_mode in {'zeros', 'reflect', 'replicate', 'circular'}:
        for layer in layer_list:
            layer.padding_mode = padding_mode
    elif padding_mode == 'median' and target_neurons is not None:
        hook_changing_neuron_out_paddings(layer_list, target_neurons, put_median_in_borders)
    elif padding_mode == 'out_reflect' and target_neurons is not None:
        hook_changing_neuron_out_paddings(layer_list, target_neurons, reflect_borders)
    else:
        print("Padding not understood", padding_mode)
        raise NotImplementedError()


def get_layer_names():
    ln = ['conv1']
    for block, subsize in enumerate([3, 4, 6, 3], start=1):
        ln += [f'conv{block}_{i}' for i in range(subsize)]
    return ln


layer_names = get_layer_names()

def put_median_in_borders(channel):
    median = channel.median()
    channel[0] = median
    channel[-1] = median
    channel[:, 0] = median
    channel[:, -1] = median
    return channel


def reflect_borders(channel):
    channel = channel[1:-1, 1:-1]
    return torch.nn.functional.pad(channel.unsqueeze(0), (1, 1, 1, 1), "reflect").squeeze()


def change_neuron_out_hook_maker(neuron_idxs, changing_function):
    show = False
    def hook(layer, input, output):
        for batch_idx in range(output.shape[0]):
            for neuron_idx in neuron_idxs:
                pan_channel_out = output[batch_idx, neuron_idx]
                if show:
                    plt.imshow(output.detach().cpu().numpy()[batch_idx, neuron_idx])
                    plt.title(f'Img: {batch_idx}, neuron: conv1_0-{neuron_idx}')
                    plt.colorbar()
                    plt.show()
                pan_channel_out = changing_function(pan_channel_out)
                output[batch_idx, neuron_idx] = pan_channel_out
                if show:
                    plt.title(f'Img: {batch_idx}, neuron: conv1_0-{neuron_idx}')
                    plt.imshow(output.detach().cpu().numpy()[batch_idx, neuron_idx])
                    plt.colorbar()
                    plt.show()
        return output
    return hook


def hook_changing_neuron_out_paddings(conv_layers, target_neurons, change_func):
    target_by_layer = {}
    for layer_name, layer in zip(layer_names, conv_layers):
        for target_layer, target_neuron in target_neurons:
            if layer_name == target_layer:
                try:
                    target_by_layer[layer_name].append(target_neuron)
                except KeyError:
                    target_by_layer[layer_name] = [target_neuron]
        try:
            print(target_by_layer[layer_name])
            layer.register_forward_hook(change_neuron_out_hook_maker(target_by_layer[layer_name], change_func))
        except KeyError:
            pass


def parse_target_neuron_file(filename):
    with open(filename, 'r') as f:
        data = [(l.split(' ')[0], int(l.split(' ')[1])) for l in f.readlines()]
    return data


def logits_to_classnames(tensor):
    class_names = []
    predictions = torch.max(tensor, 1)
    for val in predictions.indices:
        class_names.append(class_list[val])
    return class_names


def init_argparse():
    parser = argparse.ArgumentParser(
        usage="%(prog)s [PARAM1] [PARAM2]...",
        description="Extract validation set Odds for ResNet50_Weights.IMAGENET1K_V2 '\
                'with modified paddings vs original paddings.\n"\
                'Example: G.Odds.py --padding_type out_reflect --pan_file new_pans.txt -o '
                'odds.npy'
    )
    parser.add_argument(
        "-pad", "--padding_type", default='out_reflect', type=str,
        help='Type of padding to be used. Options are zeros, reflect, replicate, circular, out_reflect, median.'
             'Out_reflect uses reflect padding for PANs and zeros otherwise, and median inputs the median activation.'
             'Both require "--pan_file"'
    )
    parser.add_argument(
        "-pf", "--pan_file", default='new_pans.txt', type=str,
        help='PAN list file path. Format should be layername idx\\n per PAN, see new_pans.txt for example. Used only in' 
             'padding = out_reflect and median'
    )
    parser.add_argument(
        '-o', '--out_file', type=str,
        help="file path to output. Paper outputs stored as odds.npy and odds_rand.npy")
    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()
    padding_mode = args.padding_type
    force_cpu = False
    weights = ResNet50_Weights.IMAGENET1K_V2

    model = resnet50(weights=weights)
    model_orig = resnet50(weights=weights)
    basic_transform = weights.transforms()

    conv_layers = get_layers(model)
    if padding_mode in {'out_reflect', 'median'}:
        target_neurons = parse_target_neuron_file(args.pan_file)#'random_neurons1.txt' #"new_pans.txt"
    else:
        target_neurons = None
    model_transform_padding(conv_layers, padding_mode, target_neurons)

    if force_cpu:
        device = torch.device('cpu') # hardcoded to CPU since low-mem cuda machines explode anyway
    else:
        device = torch.device('cuda') if torch.cuda.is_available() \
                                  else torch.device('cpu')
    model = model.to(device)
    model.eval()
    model_orig = model_orig.to(device)
    model_orig.eval()



    # Get data & loader for evaluation. Run in 'original_dataset' superfolder:
    # wget https://image-net.org/data/ILSVRC/2012/ILSVRC2012_img_val.tar
    # meta.bin is also required, but we do not have permision to distribute it.
    img_net_data = torchvision.datasets.ImageNet(root=DATASETS_PATH, split='val',
                    transform=basic_transform)
    bs = 16
    data_loader = torch.utils.data.DataLoader(img_net_data, batch_size=bs,
                                              shuffle=False)

    i = 0
    smax = torch.nn.Softmax(dim=1)
    p_class_orig = torch.zeros(1000).detach().cpu()
    p_class_pan = torch.zeros(1000).detach().cpu()
    with torch.no_grad():
        for X, _ in data_loader:
            i+=1
            X = X.to(device)
            p_class_orig += smax(model_orig(X)).detach().cpu().sum(dim=0)
            p_class_pan += smax(model(X)).detach().cpu().sum(dim=0)

            if i < 0:
                break
    odds = p_class_pan/p_class_orig
    print(odds)
    # np.save('p_class_orig_rand.npy', p_class_orig.numpy())
    # np.save('p_class_pan_rand.npy', p_class_pan.numpy())
    np.save(args.out_file, odds.detach().cpu().numpy())


if __name__ == "__main__":
    main()