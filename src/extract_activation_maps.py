import os

import torchvision
import torch
from torchvision.models import resnet50, ResNet50_Weights
from torchvision import transforms
import random
import numpy as np
from src.paths import DATASETS_PATH
from matplotlib import pyplot as plt
from matplotlib import colors

def get_layers(model):
    list = [model.conv1]
    for layer_set in [model.layer1, model.layer2, model.layer3, model.layer4]:
        list += [block.conv2 for block in layer_set]
    if len(list) != len(layer_names):
        print('layer size mismatch')
    return list


def get_layer_names():
    ln = ['conv1']
    for block, subsize in enumerate([3, 4, 6, 3], start=1):
        ln += [f'conv{block}_{i}' for i in range(subsize)]
    return ln

layer_names = get_layer_names()

def parse_target_neuron_file(filename):
    with open(filename, 'r') as f:
        data = [(l.split(' ')[0], int(l.split(' ')[1])) for l in f.readlines()]
    return data

def act_hook_maker(layer_name, neuron_idx):
    target_path = os.path.join('..', 'images', f'{layer_name}_{neuron_idx}_img')
    def hook(layer, input, output):
        for batch_idx in range(output.shape[0]):
            save_path =target_path+str(batch_idx)+'.png'
            channel_out = output[batch_idx, neuron_idx].detach().cpu().numpy()
            print(channel_out)
            plt.imsave(save_path, channel_out)
        return
    return hook


def main():
    force_cpu = False
    batch_size = 2
    shuffle = True
    data_loader, device, model, weights = setup(force_cpu, batch_size=batch_size, shuffle=shuffle)
    conv_layers = get_layers(model)
    target_neurons = parse_target_neuron_file("potential_pans.txt")
    print(target_neurons)

    for layer_name, layer in zip(layer_names, conv_layers):
        for target_layer, target_neuron in target_neurons:
            if layer_name == target_layer:
                layer.register_forward_hook(act_hook_maker(target_layer, target_neuron))

    X, y = next(iter(data_loader))
    img1 = X[0].T.detach().numpy()
    img2 = X[1].T.detach().numpy()
    img1 = (img1+img1.min())/img1.max()
    img2 = (img2+img2.min())/img2.max()

    plt.imsave('tmp2', img2)
    plt.imsave('tmp1', img1)
    X = X.to(device)

    model(X)


def setup(force_cpu, batch_size=1, shuffle =False):
    weights = ResNet50_Weights.IMAGENET1K_V2
    model = resnet50(weights=weights)
    basic_transform = weights.transforms()
    if force_cpu:
        device = torch.device('cpu')  # hardcoded to CPU since low-mem cuda machines explode anyway
    else:
        device = torch.device('cuda') if torch.cuda.is_available() \
            else torch.device('cpu')
    # Get data & loader for evaluation, run in 'original_dataset' superfolder:
    # wget https://image-net.org/data/ILSVRC/2012/ILSVRC2012_img_val.tar
    img_net_data = torchvision.datasets.ImageNet(root=DATASETS_PATH, split='val',
                                                 transform=basic_transform)
    data_loader = torch.utils.data.DataLoader(img_net_data, batch_size=batch_size,
                                              shuffle=shuffle)
    model = model.to(device)
    model.eval()
    return data_loader, device, model, weights


if __name__ == "__main__":
    main()