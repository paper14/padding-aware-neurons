
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image


def get_top_k_idxs(vals, k):
    unordered_idxs = np.argpartition(vals, -k)[-k:]
    ord_idx = unordered_idxs[np.argsort(vals[unordered_idxs])]
    return ord_idx


def get_class_dict():
    with open('imagenet_classes.txt', 'r') as f:
        dic = {entry.split(' ')[0]: entry.split(' ')[2] for entry in f.read().split('\n')}
    return dic


def process_entry(entry):
    split_entry = entry.split(' ')
    return split_entry[0], float(split_entry[1]), ' '.join(split_entry[2:])



def main():
    with open('aberrant_info_ref.txt', 'r') as f:
        data = [process_entry(entry) for entry in f.read().split('\n')]
    vals = np.array([val for _, val, _ in data])
    k = 8
    top_ks = get_top_k_idxs(vals, k)
    print(vals[top_ks])
    dic = get_class_dict()
    for name, val, preds in np.array(data)[top_ks]:
        with Image.open(f'../original_dataset/val/{name}') as im:
            plt.imshow(im)
            real_name = dic[name.split('/')[0]]
            plt.title(f'{real_name} val:{val}; preds {preds}')
            plt.show()


if __name__ == '__main__':
    main()