import argparse
import pickle
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl

def plot_neuron_act_distr(acts, layer, neuron_idx, ks=[], minus=False, maxus=False, ks_plus=[],ks_minus=[]):
    #Read pre-computed data
    pad_t, pad_b, pad_r, pad_l, no_pad = [], [], [], [], []
    for activs in acts[layer]:
        pad_t += list(activs[neuron_idx][0:1].flatten())
        pad_b += list(activs[neuron_idx][-1:].flatten())
        pad_l += list(activs[neuron_idx][:,0:1].flatten())
        pad_r += list(activs[neuron_idx][:,-1].flatten())
        no_pad += list(activs[neuron_idx][1:-1,1:-1].flatten())
    no_pad.sort()
    plt.rc('ytick', labelsize=14)
    plt.rc('xtick', labelsize=14)
    mpl.rcParams['patch.linewidth'] = 3
    plt.rcParams.update({'font.size': 10})
    #"Top ($KS$="+str(ks[0])+")\n       ($KS^{+}$="+str(ks_plus[0])+")"
    #"Top ($KS$="+str(ks[0])+")\n       ($KS^{-}$="+str(ks_minus[0])+")"
    plt.hist(pad_t, bins=30, density=True, histtype='step', stacked=True, label = "Top ($KS$="+str(ks[0])[:4]+")",fc=(1, 0, 0, 0.5))
    plt.hist(pad_b, bins=30, density=True, histtype='step', stacked=True, label = "Bottom ($KS$="+str(ks[1])[:4]+")",fc=(0, 0, 0.5, 0.5))
    plt.hist(pad_l, bins=30, density=True, histtype='step', stacked=True, label = "Left ($KS$="+str(ks[2])[:4]+")",fc=(0.5, 0, 0, 0.5))
    plt.hist(pad_r, bins=30, density=True, histtype='step', stacked=True, label = "Right ($KS$="+str(ks[3])[:4]+")",fc=(0, 0.1, 0.7, 0.5))
    plt.hist(no_pad,bins=120, density=True, histtype='step', stacked=True, label = "Center",fc=(0.1, 0.1, 0.1, 0.5))
    if minus:
        plt.hist(no_pad[:len(pad_t)],bins=70, density=True, histtype='step', stacked=True, label = "$^{-}Center$",fc=(0.1, 0, 0, 0.5))
    if maxus:
        plt.hist(no_pad[-len(pad_t):],bins=70, density=True, histtype='step', stacked=True, label = "$^{+}Center$",fc=(0.1, 0, 0, 0.5))
    plt.grid()
    plt.ylim(ymin=0)
    plt.ylabel('Frequency of activations')
    plt.xlabel('Value of activation')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.show()

def print_neuron_act_map(activations, neuron_idx, size_samples = 5):
    np.set_printoptions(precision=2, linewidth=120)
    print('\n')
    print('Neuron:',neuron_idx)
    counter = 0
    for sample_data in activations:
        if counter >= size_samples:
            break
        counter+=1   
        print(sample_data[neuron_idx][-5:-1])
        print('\n')

def read_data(name):
    #Read pre-computed data
    kstest_data = {}
    with open(name, 'rb') as f:
        kstest_data = pickle.load(f)
    return kstest_data

def kstest_data(filename, layer,idx):
    from decimal import Decimal, Context
    kstest_data = read_data(filename)
    ks_values = []
    ks_values.append(Context(prec=2).create_decimal(kstest_data[layer][idx][0][0]))
    ks_values.append(Context(prec=2).create_decimal(kstest_data[layer][idx][1][0]))
    ks_values.append(Context(prec=2).create_decimal(kstest_data[layer][idx][2][0]))
    ks_values.append(Context(prec=2).create_decimal(kstest_data[layer][idx][3][0]))
    return ks_values

def read_activations(neuron_activations_file):
    #Read pre-computed data
    activations_dict = {}
    with open(neuron_activations_file, 'rb') as f:
        activations_dict = pickle.load(f)
    return activations_dict

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [PARAM1] [PARAM2]...",
        description="Extract activations produced by a batch of data in '\
            'a pre-trained model given a set of parameters."
    )
    parser.add_argument(
        "-model", "--model_architecture", default = 'resnet50', type = str, \
        help = 'Pre-trained model to be used. Options are resnet50, densenet121,'\
            ' mobilenet_v3_large, googlenet, fasterrcnn, fcos_resnet'
    )
    parser.add_argument(
        "-pad", "--padding_type", default = 'zeros', type = str, \
        help = 'Type of padding to be used. Options are zeros, reflect, replicate and circular'
    )
    parser.add_argument(
        "-nl", "--name_layer", type = str, required = True,\
        help = 'Name of layer where the neuron of interest is'
    )
    parser.add_argument(
        "-nn", "--number_neuron", type = int, required = True, \
        help = 'Index of neuron of interest'
    )
    parser.add_argument(
        "-max", "--add_maximus", action = 'store_true', \
        help = 'Add truncated center distribution focus on the positive side'
    )
    parser.add_argument(
        "-min", "--add_minus", action = 'store_true', \
        help = 'Add truncated center distribution focus on the negative side'
    )
    parser.add_argument(
        "-ks", "--kolmogorov_smirnov_type", default = 'two-sided', type = str, \
        help = 'Type of Kolmogorov-Smirnov test to be used. Options are two-sided, less, greater'
    )
    parser.add_argument(
        "-tr", "--truncated", default = 'complete', type = str, \
        help = 'Truncated central distribution or complete. Options are complete, bottom and top.'
    )
    parser.set_defaults(add_minus=False, add_maxus=False)
    return parser

#MAIN
def main():
    #parse params
    parser = init_argparse()
    args = parser.parse_args()
    data_filename = args.model_architecture+'_activations_'+args.padding_type +\
        '.pkl'
    ks_filename = 'ks_'+args.model_architecture+'_'+args.truncated+\
        '_'+args.kolmogorov_smirnov_type+'_'+args.padding_type+'.pkl'
    #Compute KS data, for the labels of the plot
    ks_neuron = kstest_data(ks_filename, args.name_layer, args.number_neuron)
    #Generate plot
    activations_dict = read_activations(data_filename)
    if args.name_layer not in activations_dict.keys():
        raise Exception('Layer '+str(args.name_layer)+' not found in set:',\
            str(activations_dict.keys()))
    plot_neuron_act_distr(activations_dict, args.name_layer,args.number_neuron,\
        ks_neuron, args.add_minus, args.add_maxus)
    return

if __name__ == "__main__":
    main()