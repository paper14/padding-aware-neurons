import argparse
from os import stat
from scipy import stats
import pickle

#The purpose of this method is to remove the samples axis, creating a distribution of activations per neuron
#Input is a list of 3D tensors, 1st ax neuron, 2nd & 3rd are 2D spatial ax
#Returns a list of len num. neurons, for each, 5 lists of activations from all samples
def compute_pad_area_dist(activations):
    num_neurons = activations.shape[1]
    stats = []
    for neuron_idx in range(num_neurons):
        pad_t, pad_b, pad_l, pad_r, no_pad = [], [], [], [], []
        for sample in activations:
            pad_t += list(sample[neuron_idx][0:1].flatten())
            pad_b += list(sample[neuron_idx][-1:].flatten())
            pad_l += list(sample[neuron_idx][:,0:1].flatten())
            pad_r += list(sample[neuron_idx][:,-1:].flatten())
            no_pad += list(sample[neuron_idx][1:-1,1:-1].flatten())
        stats.append((pad_t, pad_b, pad_l, pad_r, no_pad))
    return stats

def ksdistances_layer(acts, layer_name, ks_type, trunc):
    print('Computing KS on layer',layer_name,', which contains',len(acts),'neurons')
    kstest_all_neurons = []
    for neuron in acts:
        border_act_size = len(neuron[0])
        neuron[4].sort()
        neuron_center = neuron[4]
        if trunc == -1: #keep lower values of center distrib.
            neuron_center = neuron_center[:border_act_size] #lowest values
        if trunc == 1: #keep higher values of center distrib.
            neuron_center = neuron_center[-border_act_size:] #higher values
        #Compute KS. This may crash because of memory if batch is too large
        kstest_top = stats.kstest(neuron[0], neuron_center, alternative=ks_type)        
        kstest_bot = stats.kstest(neuron[1], neuron_center, alternative=ks_type)
        kstest_lef = stats.kstest(neuron[2], neuron_center, alternative=ks_type)
        kstest_rig = stats.kstest(neuron[3], neuron_center, alternative=ks_type)
        #Store
        kstest_all_neurons.append((kstest_top, kstest_bot, kstest_lef, kstest_rig))
    return kstest_all_neurons 

def read_activations(model_arch, padding_mode):
    #Read pre-computed activations
    activations_dict = {}
    with open(model_arch+'_activations_'+padding_mode+'.pkl', 'rb') as f:
        activations_dict = pickle.load(f)
    return activations_dict

def get_distributions(activations_dict):
    #Get data distributions per neuron, merging all input samples
    neuron_statistics = {}
    for layer in activations_dict:
        neuron_statistics[layer] = compute_pad_area_dist(activations_dict[layer])
    return neuron_statistics

def compute_4_distances(neuron_statistics, ks_type, trunc):
    #Compute 4 distances (top, bot, lef, rig vs center) per neuron 
    ks_distances = {}
    for layer in neuron_statistics:
        ks_distances[layer] = ksdistances_layer(neuron_statistics[layer], layer,\
            ks_type, trunc)
    return ks_distances

def persist(trunc, model_arch, ks_type, padding_mode, ks_distances):
    #Persist
    if trunc==-1:
        truncated = 'bottom'
    if trunc==0:
        truncated = 'complete'
    if trunc==1:
        truncated = 'top'    
    with open('ks_'+model_arch+'_'+truncated+'_'+ks_type+'_'+padding_mode+'.pkl', 'wb') as f:
        pickle.dump(ks_distances, f)
    return

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [PARAM1] [PARAM2]...",
        description="Extract activations produced by a batch of data in '\
            'a pre-trained model given a set of parameters."
    )
    parser.add_argument(
        "-model", "--model_architecture", default = 'resnet50', type = str, \
        help = 'Pre-trained model to be used. Options are resnet50, densenet121,'\
            ' mobilenet_v3_large, googlenet, fasterrcnn, fcos_resnet'
    )
    parser.add_argument(
        "-pad", "--padding_type", default = 'zeros', type = str, \
        help = 'Type of padding to be used. Options are zeros, reflect, replicate and circular'
    )
    parser.add_argument(
        "-ks", "--kolmogorov_smirnov_type", default = 'two-sided', type = str, \
        help = 'Type of Kolmogorov-Smirnov test to be used. Options are two-sided, less, greater'
    )
    return parser

#MAIN
def main():
    #parse params
    parser = init_argparse()
    args = parser.parse_args()
    # Truncate central distribution based on KS typeon the negative side (-1), on the positive (+1) or no trunc (0).
    if args.kolmogorov_smirnov_type == 'two-sided':
        ktrunc = 0 #(for finding edge detector candidates, no truncate)
    elif args.kolmogorov_smirnov_type == 'less':
        ktrunc = 1 # (for finding PAN candidates, keep the most positive side only)
    elif args.kolmogorov_smirnov_type == 'greater':
        ktrunc = -1 # (for finding PAN candidates, keep the most negative side only)
    #Do your thing
    activations_dict = read_activations(args.model_architecture, args.padding_type)
    neuron_statistics = get_distributions(activations_dict)
    ks_distances = compute_4_distances(neuron_statistics, args.kolmogorov_smirnov_type, ktrunc)
    persist(ktrunc, args.model_architecture, args.kolmogorov_smirnov_type, args.padding_type, ks_distances) 
    return

if __name__ == "__main__":
    main()